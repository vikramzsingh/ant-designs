import './App.css';
// import ButtonComponents from './components/ButtonComponents';
import CardComponent from './components/CardComponent';
import FormComponent from './components/FormComponent';
import FormComp from './components/forms/FormComp';
import ModelComponent from './components/ModelComponent';
import ParentComponent from './components/ParentComponent';
import ProgressComponent from './components/ProgressComponent';
import TableComponent from './components/TableComponent';
import TabsComponent from './components/TabsComponent';

function App() {
  return (
    <div className="App">
      {/* <ButtonComponents />
      <ParentComponent />
      <CardComponent />
      <ModelComponent />
      <FormComponent />
      <TableComponent />
      <TabsComponent />
      <ProgressComponent /> */}
      <FormComp />
    </div>
  );
}

export default App;
