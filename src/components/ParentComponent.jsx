import React from "react";
import { Layout } from "antd";
import AppHeader from "./AppHeader";
import "../assets/CssStyles/AppHeaderStyle.css";
import ContentComponent from "./ContentComponent";

const { Header, Content } = Layout;

const ParentComponent = () => {
  return (  
    <Layout className="layout">
      <Header className="header">
        <AppHeader />
      </Header>
      <Content style={{ padding: '0px 50px 50px 50px'}}>
        <ContentComponent />
      </Content>
    </Layout>
  );
};

export default ParentComponent;
