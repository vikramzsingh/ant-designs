import React from "react";
import {
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  Radio,
  Row,
  Select,
  Space,
  Switch,
  Tooltip,
  Typography,
} from "antd";
import Checkbox from "antd/lib/checkbox/Checkbox";
import styled from "styled-components";
import { Option } from "antd/lib/mentions";
import styles from "../../assets/CssStyles/AppHeaderStyle.css";

const { Item } = Form;
const { Password } = Input;

const StyledForm = styled(Form)`
  border: 1px solid #000000;
`;

const StyleItem = styled(Item)`
  & label {
    font-size: 20px;
    font-weight: bold;
  }
`;

const StyledPassword = styled(Password)`
  border: 1px solid #a4a4a4;
  border-radius: 8px solid #a4a4a4;

  &&:hover {
    border: 1px solid#9effff;
    box-shadow: 4px 4px#00e676;
  }
  &:focus {
    border: 1px solid#00e676;
  }
`;

const StyledInput = styled(Input)`
  border: 1px solid #a4a4a4;
  border-radius: 8px solid #a4a4a4;

  &:hover {
    border: 1px solid#9effff;
    box-shadow: 4px 4px#00e676;
  }
  &:focus {
    border: 1px solid#00e676;
  }
`;

const formLayout = {
  labelCol: {
    span: 8,
    offset: 8,
  },
  wrapperCol: {
    span: 8,
    offset: 8,
  },
};

const formTailLayout = {
  wrapperCol: {
    span: 8,
    offset: 8,
  },
};

const FormComp = () => {
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      <StyledForm
        {...formLayout}
        layout="vertical"
        name="basic"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <StyleItem
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <StyledInput />
        </StyleItem>

        <StyleItem
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <StyledPassword />
        </StyleItem>

        <StyledForm.Item name="remember">
          <Checkbox>Remember</Checkbox>
        </StyledForm.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>

        <Form.Item label="DatePicker">
          <DatePicker />
        </Form.Item>

        <Form.Item>
          <Radio>Select Radio 1</Radio>
          <Radio>Select Radio 2</Radio>
        </Form.Item>

        <Row>
          <Col offset={8}>
            <Radio.Group>
              <Radio value="small">Small</Radio>
              <Radio value="default">Default</Radio>
              <Radio.Button value="large">Large</Radio.Button>
            </Radio.Group>
          </Col>
        </Row>

        <Form.Item label="Switch">
          <Switch />
        </Form.Item>

        <Form.Item label="Text Area">
          <Input.TextArea />
        </Form.Item>

        <Form.Item label="Username">
          <Space size="middle">
            <Form.Item>
              <Input />
            </Form.Item>
            <Form.Item>
              <Tooltip title="Useful Information">
                <Typography.Link href="!#">Need Help?</Typography.Link>
              </Tooltip>
            </Form.Item>
          </Space>
        </Form.Item>

        <Form.Item label="Address">
          <Input.Group compact>
            <Form.Item
              name={["address", "province"]}
              noStyle
              rules={[{ required: true, message: "Province is required" }]}
            >
              <Select placeholder="Select province">
                <Option value="Zhejiang">Zhejiang</Option>
                <Option value="Jiangsu">Jiangsu</Option>
              </Select>
            </Form.Item>
            <Form.Item
              name={["address", "street"]}
              noStyle
              rules={[{ required: true, message: "Street is required" }]}
            >
              <Input style={{ width: "50%" }} placeholder="Input street" />
            </Form.Item>
          </Input.Group>
        </Form.Item>
      </StyledForm>
    </div>
  );
};

export default FormComp;
