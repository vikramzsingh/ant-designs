import React from "react";
import { Row, Menu, Col } from "antd";
import "../assets/CssStyles/AppHeaderStyle.css";

const AppHeader = () => {
  return (
    <>
      <Row>
        <Col sm={6} xs={6}>
          <div className="logo">
            <i className="fas fa-bolt"></i>
            <i className="fa fa-cloud" aria-hidden="true"></i>
          </div>
        </Col>
        <Col sm={9} xs={12} offset={6}>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
            <Menu.Item key="1">nav 1</Menu.Item>
            <Menu.Item key="2">nav 2</Menu.Item>
            <Menu.Item key="3">nav 3</Menu.Item>
          </Menu>
        </Col>
      </Row>
    </>
  );
};

export default AppHeader;
