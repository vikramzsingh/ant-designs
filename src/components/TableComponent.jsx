import React from "react";
import { Table, Tag, Space } from "antd";

// Specifying table heading of Lable
const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age",
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address",
  },
  {
    title: "Tags",
    dataIndex: "tags",
    key: "tags",
    render: tags => (
        <>
            {tags.map((tag) => {
                let color = tag.length < 5 ? "geekblue" : "yellow"
                if(tag === "loser") {
                    color = "volcano"
                }
                return(
                    <Tag color={color} closable key={tag}>
                        {tag.toUpperCase()}
                    </Tag>
                )
            })
            }
        </>
    )
  },
  {
    title: "Action",
    // dataIndex: "action", data is note from datastore so we can leave  this
    key: "action",
    render: (text, record) => (
        <Space size="middle">
            <a href="!#">Invite {record.name}</a>
            <a href="!#">Delete</a>
        </Space>
    )
  },
];

const data = [
    {
        key: "1",
        name: "James",
        age: "34",
        address: "New York",
        tags: ["nice", "developer"]
    },
    {
        key: "2",
        name: "Bond",
        age: "32",
        address: "London",
        tags: ["Agent"]
    }
]

const TableComponent = () => {
  return (
    <>
      <Table columns={columns} dataSource={data} pagination={false}/>
    </>
  );
};

export default TableComponent;
