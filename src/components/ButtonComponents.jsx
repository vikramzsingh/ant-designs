import React from 'react';
import { Button } from 'antd';

const ButtonComponents = () => {
    return (
        <div>
            <Button type="primary">Primary Button</Button>
        </div>
    );
}

export default ButtonComponents;
