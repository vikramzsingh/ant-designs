import { Card } from "antd";
import React from "react";

const { Meta } = Card;

const CardComponent = () => {
  return (
    <>
      <Card
        bordered={false}
        hoverable
        title="Default size card"
        extra={<a href="!#">More</a>}
        style={{ width: 300 }}
      >
        <p>Card content</p>
        <p>Card content</p>
        <p>Card content</p>
        <Meta title="Europe Street beat" description="www.instagram.com" />
      </Card>
      <Card
        hoverable
        size="small"
        title="Small size card"
        extra={<a href="!#">More</a>}
        style={{ width: 300 }}
      >
        <p>Card content</p>
        <p>Card content</p>
        <p>Card content</p>
      </Card>
    </>
  );
};

export default CardComponent;
