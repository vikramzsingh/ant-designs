import React from "react";
import { Breadcrumb } from "antd";
import "../assets/CssStyles/AppHeaderStyle.css";
import HeroComp from "./HeroComp";

const ContentComponent = () => {
  return (
    <>
      <Breadcrumb style={{ margin: "16px 0px 0px 0px" }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>List</Breadcrumb.Item>
        <Breadcrumb.Item>App</Breadcrumb.Item>
      </Breadcrumb>
      <div className="site-layout-content">
        <HeroComp />
      </div>
    </>
  );
};

export default ContentComponent;
