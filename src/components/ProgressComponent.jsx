import { Progress } from "antd";
import React from "react";

const ProgressComponent = () => {
  return (
    <>
      <Progress percent={30} />
      <Progress percent={50} status="active" />
      <Progress percent={70} status="exception" />
      <Progress percent={90} strokeColor="blue" strokeWidth={2} />
      <Progress percent={100} showInfo={false} />
      <>
        <Progress type="circle" percent={75} />
        <Progress type="circle" percent={70} status="exception" />
        <Progress type="circle" percent={100} />
      </>
      <>
        <Progress
          type="circle"
          percent={75}
          format={(percent) => `${percent} Days`}
        />
        <Progress type="circle" percent={100} format={() => "Done"} />
      </>
    </>
  );
};

export default ProgressComponent;
