import React from "react";
import { Tabs } from "antd";

const { TabPane } = Tabs;

const callback = (key) => {
    console.log(key)
}

const TabsComponent = () => {
  return (
    <>
      <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="tab 1" key="1">
            Content of Tab Pane 1
          </TabPane>
          <TabPane tab="Tab 2" key="2">
            Content of Tab Pane 2
          </TabPane>
          <TabPane tab="Tab 3" key="3">
            Content of Tab Pane 3
          </TabPane>
      </Tabs>
      <Tabs defaultActiveKey="1" onChange={callback} tabPosition="left">
          <TabPane tab="tab 1" key="1">
            Content of Tab Pane 1
          </TabPane>
          <TabPane tab="Tab 2" key="2">
            Content of Tab Pane 2
          </TabPane>
          <TabPane tab="Tab 3" key="3">
            Content of Tab Pane 3
          </TabPane>
      </Tabs>
    </>
  );
};

export default TabsComponent;
